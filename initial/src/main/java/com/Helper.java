package com;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Helper {
    private static HttpServletRequest _request;

    public Helper(HttpServletRequest request){
        this._request= request;
    }

    public String getHostURL(){
        return _request.getRequestURL().toString();
    }

    public String getTargetURL(){
        String url = System.getenv("TARGET_SITE");
        return url;
    }

    public ArrayList<String> getSitemapList() {
        ArrayList<String> urls = new ArrayList<>();

        try{
            String endpoint = System.getenv("TARGET_SITE");
            Document doc = Jsoup.connect(endpoint).get();
            Elements urlsElements = doc.select("loc");

            for (Element url : urlsElements) {
                urls.add(url.text());
            };

            return urls;
        }
        catch(Exception e){
            return null;
        }
    };

    public HashMap<String, String> getSitemapTitleURLHashMap() {
        HashMap<String, String> urlsHash = new HashMap<>();

        try{
            String endpoint = System.getenv("TARGET_SITE");
            Document doc = Jsoup.connect(endpoint).get();
            Elements urlsElements = doc.select("loc");

            for (Element urlObj : urlsElements) {
                String[] urlArray = urlObj.text().split("/");
                String title = urlArray[urlArray.length - 1];
                String url = urlObj.text();
                urlsHash.put(title, url);
            };

            return urlsHash;
        }
        catch(Exception e){
            return null;
        }
    };

    public String flipWebsite(){
        String css = "<style>  body{   margin: 0 auto;\n" +
                "        -moz-transform: scaleX(-1);\n" +
                "        -o-transform: scaleX(-1);\n" +
                "        -webkit-transform: scaleX(-1);\n" +
                "        transform: scaleX(-1);\n" +
                "        filter: FlipH;\n" +
                "        -ms-filter: \"FlipH\";}</style>";
        return css;
    }

    public String invertWebsite(){
        return "<style>body{ filter: invert(100%);}</style>";
    }

    public Document substituteContent(Document content, String replace ){
        Element body = content.body();

        for (Element elem : body.getAllElements()) {
            String[] validTags = {"p","a","small","strong","h1","h2","h3","h4","h5","span"};

            boolean valid = Arrays.asList(validTags).contains(elem.tagName());

            if(valid){
                String[] arrayText = elem.text().split(" ");
                String finalText = "";
                for (String text: arrayText) {
                    finalText += replace + " ";
                }
                elem.text(finalText);
            }
        }
        return content;
    }

    public Document addQueryString(Document doc, String query){
        Element body = doc.body();
        for (Element elem : body.getAllElements()) {
            if(elem.tagName() == "a"){
                String url = elem.attr("href") + query;
                elem.attr("href", url);
            }
        }
        return doc;
    }
}
