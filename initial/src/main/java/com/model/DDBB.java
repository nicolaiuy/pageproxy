package com.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class DDBB {
    private Connection conn;

    public DDBB(String url, String user ,String pwd){
        try{
            this.conn = DriverManager.getConnection(url, user, pwd);
        }catch(Exception e){
            System.out.println("Error stablishing a database connection my dude");
        }
    }

    public String getContent() {
        ArrayList<String> rows = new ArrayList<>();
        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM websites");

            while(result.next()){
                String name = result.getString("name");
                String url = result.getString("url");
                rows.add(name + " - " + url);
            };
        }catch(Exception e){
        }

        return rows.toString();
    }
}
