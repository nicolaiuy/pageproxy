package com.xmartlabs;

import com.Helper;
import com.model.DDBB;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
public class PageController {

    private static final String page = "";

    @GetMapping("/")
    public String home(
            HttpServletRequest request,
            @RequestParam(value = "invert", defaultValue = "false") boolean invert,
            @RequestParam(value = "flip", defaultValue = "true") boolean flip,
            @RequestParam(value = "evil", defaultValue = "false") boolean evil,
            @RequestParam(value = "replace", defaultValue = "") String replace
    ) {
        Helper helper = new Helper(request);
        ArrayList<String> queryArray = new ArrayList();

        try{
            System.out.println(helper.getTargetURL());
            Document doc = Jsoup.connect(helper.getTargetURL()).get();

            if(invert){
                doc.append(helper.invertWebsite());
                queryArray.add("invert=true");
            }

            if(flip){
                doc.append(helper.flipWebsite());
                queryArray.add("flip=true");
            }

            if(evil){
                doc.append(helper.invertWebsite());
                queryArray.add("evil=true");
            }

            if(replace.length() > 0){
                doc = helper.substituteContent(doc, replace);
                queryArray.add("replace=" + replace);
            }

            return doc.toString();
        }catch(Exception e){
            System.out.println(e);
            return "External Server error";
        }

    }

    @GetMapping("/{page}")
    public String page(@PathVariable String page,
                       @RequestParam(value = "invert", defaultValue = "false") boolean invert,
                       @RequestParam(value = "flip", defaultValue = "true") boolean flip,
                       @RequestParam(value = "evil", defaultValue = "false") boolean evil,
                       @RequestParam(value = "replace", defaultValue = "") String replace,
                       HttpServletRequest request)
    {

        Helper helper = new Helper(request);
        HashMap<String, String> urls = helper.getSitemapTitleURLHashMap();
        ArrayList<String> queryArray = new ArrayList();

        // If page exists
        if(urls.containsKey(page)){
            try{
                Document doc  = Jsoup.connect(urls.get(page)).get();

                // Check Querystring

                if(invert){
                    doc.append(helper.invertWebsite());
                    queryArray.add("invert=true");
                }

                if(flip){
                    doc.append(helper.flipWebsite());
                    queryArray.add("flip=true");
                }

                if(evil){
                    doc.append(helper.invertWebsite());
                    queryArray.add("evil=true");
                }

                if(replace.length() > 0){
                    doc = helper.substituteContent(doc, replace);
                    queryArray.add("replace=" + replace);
                }

                // Apply Querystring to anchors
                String query = queryArray.size() < 1 ? "" : "?" + String.join("&", queryArray);
                doc =  helper.addQueryString(doc,query);

                return doc.toString();

            }catch(Exception e){
                return "Error: external service unavailable";
            }
        }else {
            return "Error: page dosen't exist";
        }

    }

    @GetMapping("/sitemap")
    public String sitemap(HttpServletRequest request) {
        Helper helper = new Helper(request);
        ArrayList<String> urls = helper.getSitemapList();
        Document response = null;
        String html = "";

        String url = helper.getHostURL();

        for(String _url : urls){
            String[] urlArray = _url.split("/");
            String title = urlArray[urlArray.length - 1];

            html += Jsoup.parse("<div style='margin-bottom: 5pt'>");
            html += Jsoup.parse("<a href='"+ url + title +"' style='margin-right: 5pt'>"+ title +"</a>");
            html += Jsoup.parse("<a href='"+ url + title +"?flip=false'>unflip</a>");
            html += Jsoup.parse("<a href='"+ url + title +"?evil=true'>evil</a></br>");
            html += Jsoup.parse("</div>");
        }

        response = Jsoup.parse(html);
        return response.toString();
    }

    @GetMapping("/db")
    public String testDDBB(){
        String url = System.getenv("DDBB_URL");
        String user = System.getenv("DDBB_USER");
        String pwd = System.getenv("DDBB_PWD");

        DDBB ddbb = new DDBB(url, user, pwd);
        return ddbb.getContent();
    }

    @GetMapping("/test")
    public String test(HttpServletRequest request){
        Helper helper = new Helper(request);
        return "OK - " + helper.getTargetURL();
    }

}